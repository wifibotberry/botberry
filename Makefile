.PHONY: all install uninstall
all:	termcontrol wifibotserver

termcontrol: servocommand.c termcontrol.c
	gcc -Wall termcontrol.c -o termcontrol

wifibotserver: servocommand.c wifibotserver.c
	gcc -Wall wifibotserver.c -o wifibotserver

install: wifibotserver
	[ "`id -u`" = "0" ] || { echo "Must be run as root"; exit 1; }
	cp -f wifibotserver /usr/local/sbin
	cp -f init-script /etc/init.d/wifibotserver
	chmod 755 /etc/init.d/wifibotserver
	update-rc.d wifibotserver defaults 92 08
	/etc/init.d/wifibotserver start

uninstall:
	[ "`id -u`" = "0" ] || { echo "Must be run as root"; exit 1; }
	[ -e /etc/init.d/wifibotserver ] && /etc/init.d/wifibotserver stop || :
	update-rc.d wifibotserver remove
	rm -f /usr/local/sbin/wifibotserver
	rm -f /etc/init.d/wifibotserver

clean:
	rm -f wifibotserver
	rm -f termcontrol