These software are under GNU GPLv3 license

termcontrol allows remote control of the BerryBot through any console.
wifibotserver allows control of the BerryBot with any application supporting old WifiBot protocol.

Both of them presume that your robot is build from a 2 driving wheel model using hacked servomotors, driven by the ServoBlaster software. The left motor is assumed to have the number 0 in ServoBlaster (GPIO4 with original ServoBlaster configuration), while the right motor is assumed to have the number 1 (GPIO17 with original ServoBlaster configuration)

==GETTING IT TO WORK==
edit the IDLELEFT and IDLERIGHT values in servocommand.c file to match your servomotors standby values

run "make" to compile, then "sudo make install" to make the wifibotserver start automatically at boot

==TERMCONTROL==
if you use a qwerty keyboard, replace the "z","s","q" and "d" values in the termcontrol.c file by "w","s","a" and "d".

==WIFIBOTSERVER==

the wifibotserver listens on the port 15020 TCP, and uses a very simple protocol : to control the bot, just send 2 bytes, the first bytes stands for the left wheel, while the seconds is for the right wheel. Both must be coded the following way : 1DSSSSSS where D is the direction of the wheel ( 1 = forward, 0 = backward ), and SSSSSS is the speed of the wheel, from 0 to 63. To stop the wheel, simply set the speed to 0. 

Examples :
full speed forward : send 0b11111111 and 0b11111111
turning left, full speed : send 0b10111111 and 0b11111111 