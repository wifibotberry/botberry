#include <stdio.h>
#include <termio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "servocommand.c"


/* cette fonction reconfigure le terminal, et stocke */
/* la configuration initiale a l'adresse prev */
int reconfigure_terminal (struct termios *prev)
{
    struct termios new;
    if (tcgetattr(fileno(stdin),prev)==-1) {
        perror("tcgetattr");
        return -1;
    }
    new.c_iflag=prev->c_iflag;
    new.c_oflag=prev->c_oflag;
    new.c_cflag=prev->c_cflag;
    new.c_lflag=0;
    new.c_cc[VMIN]=1;
    new.c_cc[VTIME]=0;
    if (tcsetattr(fileno(stdin),TCSANOW,&new)==-1) {
        perror("tcsetattr");
        return -1;
    }
    return 0;
}

/* cette fonction restaure le terminal avec la */
/* configuration stockee a l'adresse prev */
int restore_terminal (struct termios *prev)
{
    return tcsetattr(fileno(stdin),TCSANOW,prev);
}

/* exemple d'utilisation */
int main (int argc,char *argv[])
{
    struct termios prev;
    int exitLoop;
    int sb;


    //opening servoblaster device
    sb = open("/dev/servoblaster",O_WRONLY|O_NONBLOCK);
    if(sb==-1){
        perror("Failed to open servoblaster device ");
        return(-1);
    }

    //reconfiguring terminal (non canonical mode)
    if (reconfigure_terminal(&prev)==-1){
        perror("Failed to reconfigure terminal ");
        return -1;
    }
    printf("Terminal reconfigured, press BACKSPACE to quit\n");

    exitLoop=0;
    while(exitLoop==0){
        switch(getchar()){
        case 127:   //if backspace, exit
            exitLoop=1;
            break;
        case 'z':
            servoCommand(sb,50,50);
            break;
        case 's':
            servoCommand(sb,-50,-50);
            break;
        case 'q':
            servoCommand(sb,-50,50);
            break;
        case 'd':
            servoCommand(sb,50,-50);
            break;
        }
    }

    //restoring terminal configuration
    if (restore_terminal(&prev)==-1){
        perror("Failed to restore terminal configuration ");
        return -1;
    }
    printf("End of program\n");
    servoCommand(sb,0,0);
    close(sb);
    return 0;
}
