//left and right servo idle values
#define IDLELEFT    253
#define IDLERIGHT   249
//#define INVERTDIRECTION


//this function send the commands to servoblaster
int servoCommand(int servoblasterFileDescriptor, int leftCommand, int rightCommand){
#ifdef INVERTDIRECTION
    return dprintf(servoblasterFileDescriptor,"0=%d\n1=%d\n",IDLELEFT-leftCommand,IDLERIGHT+rightCommand);
#else
    return dprintf(servoblasterFileDescriptor,"0=%d\n1=%d\n",IDLELEFT+leftCommand,IDLERIGHT-rightCommand);
#endif
}
