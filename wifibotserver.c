#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "servocommand.c"

#define BYTE_TO_READ    2
#define PORT            15020



int main()
{

    //Network variables
    int sock_descriptor, conn_desc;
    struct sockaddr_in serv_addr, client_addr;
    char buff[BYTE_TO_READ+1];
    char client_addr_str[INET_ADDRSTRLEN];
    int readValue;

    //Servoblaster device file descriptor
    int sb = 0;

    //Bot control variables
    int leftSpeed=0;
    int rightSpeed=0;

    //turning process into daemon
    if(daemon(0,1) < 0){
       perror("servod: Failed to daemonize process: %m\n");
       return(-1);
    }

    //Opening servoblaster file descriptor
    sb = open("/dev/servoblaster",O_WRONLY|O_NONBLOCK);
    if(sb==-1){
        perror("Unable to open servoblaster device ");
        return(-1);
    }

    //Creating socket descriptor

    sock_descriptor = socket(AF_INET, SOCK_STREAM, 0);

    if(sock_descriptor < 0){
        perror("Failed creating socket\n");
        return(-1);
    }

    //Filling the server address / port structure

    bzero((char *)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(PORT);

    //Binding the socket descriptor the the address / port

    if (bind(sock_descriptor, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        printf("Failed to bind\n");

    //Main loop

    while(1){

        //Listening for incoming connection. Only accept the current connection ( 0 more client accepted )
        listen(sock_descriptor, 0);
        printf("Waiting for connection...\n");

        //When client connects, establish the connection
        unsigned int size = sizeof(client_addr);
        conn_desc = accept(sock_descriptor, (struct sockaddr *)&client_addr, &size);

        if (conn_desc == -1){
            printf("Failed accepting connection\n");
        }

        //If connection established...
        else{

            //Convert the client address to string to be displayed in log
            inet_ntop(AF_INET,&(client_addr.sin_addr),client_addr_str,INET_ADDRSTRLEN);
            printf("%s : Connection established \n",client_addr_str);

            //This loop occurs while the client stay connected
            do{

                //Check for incoming data
                readValue=read(conn_desc, buff, sizeof(buff)-1);

                if(readValue==BYTE_TO_READ){

                    //Process the left speed value
                    leftSpeed=((int)buff[0]&0b00111111)*50/63;
                    if((buff[0]&0b11000000)==0b10000000)leftSpeed*=-1;

                    //Process the right speed value
                    rightSpeed=((int)buff[1]&0b00111111)*50/63;
                    if((buff[1]&0b11000000)==0b10000000)rightSpeed*=-1;
                }


                //Always send to servoblaster, else it would timeout and shut the servo down
                servoCommand(sb,leftSpeed,rightSpeed);
            }while(readValue!=0);

            //closing connection and resetting all values
            close(conn_desc);
            printf("%s : Disconnected\n",client_addr_str);
            servoCommand(sb,0,0);
            leftSpeed=0;rightSpeed=0;
        }
    }
    close(sock_descriptor);
    close(sb);
    return 0;
}
